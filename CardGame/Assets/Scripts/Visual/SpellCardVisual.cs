﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpellCardVisual : CardVisual
{
    public SpellCardAsset CardAsset;
    public override CardAsset asset
    {
        get
        {
            return CardAsset;
        }
        set
        {
            CardAsset = value as SpellCardAsset;
        }
    }
}
