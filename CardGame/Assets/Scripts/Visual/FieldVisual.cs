﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class FieldVisual : UnityEngine.MonoBehaviour {

    List<GameObject> cardsInField = new List<GameObject>();

    public SameDistanceChildren slots;

    public void AddCard(CreatureCardLogic card, PlayerLogic player)
    {
        CreateCard(card, player);
        foreach (GameObject g in cardsInField)
        {
            g.transform.DOMove(slots.Children[cardsInField.IndexOf(g)].position,0.5f);
        }
        CommandManager.instance.SkipCommand();
    }

    public void AddCard(CardAsset card, PlayerLogic player)
    {
        CreateCard(card, player);
        foreach (GameObject g in cardsInField)
        {
            g.transform.DOMove(slots.Children[cardsInField.IndexOf(g)].position, 0.5f);
        }
        CommandManager.instance.SkipCommand();
    }


    void CreateCard(CreatureCardLogic card, PlayerLogic player)
    {
        GameObject c = PoolManager.instance.GetCreature();
        c.transform.position = GlobalSettings.instance.PlayPreview.position;
        CreatureLogic cl = c.GetComponent<CreatureLogic>();
        CreatureVisual cv = c.GetComponent<CreatureVisual>();
        cv.asset = card.asset as CreatureCardAsset;
        cv.LoadAssets();
        cl.Load(cv, player,card.effect);
        cl.Action();
        player.field.field.Add(cl);
        c.SetActive(true);
        cardsInField.Add(c);
    }

    void CreateCard(CardAsset card, PlayerLogic player)
    {
        GameObject cardL = PoolManager.instance.GetCreatureCard();
        GameObject c = PoolManager.instance.GetCreature();
        
        CreatureCardLogic ccl = cardL.GetComponent<CreatureCardLogic>();
        CreatureLogic cl = c.GetComponent<CreatureLogic>();
        CreatureVisual cv = c.GetComponent<CreatureVisual>();
        ccl.Load(card, player);
        cv.asset = card as CreatureCardAsset;
        cv.LoadAssets();
        cl.Load(cv, player, ccl.effect);
        //cl.Action();
        player.field.field.Add(cl);
        c.SetActive(true);
        cardsInField.Add(c);
        c.transform.position = slots.Children[cardsInField.IndexOf(c)].position;
    }

    public void RemoveCard(GameObject card)
    {
        cardsInField.Remove(card);
    }
}
