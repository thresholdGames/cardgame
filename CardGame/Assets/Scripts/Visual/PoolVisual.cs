﻿    using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PoolVisual : UnityEngine.MonoBehaviour {


    public Image[] Crystals;
    public Text ProgressText;

    private int totalcrystals;
    public int TotalCrystals
    {
        get { return totalcrystals; }
        set
        {
            if (value >= Crystals.Length)
                totalcrystals = Crystals.Length;
            else if (value < 0)
                totalcrystals = 0;
            else
                totalcrystals = value;

            for (int i = 0; i < Crystals.Length; i++)
            {
                if (i < totalcrystals)
                {
                    if (Crystals[i].color == Color.clear)
                        Crystals[i].color = Color.gray;
                }
                else
                    Crystals[i].color = Color.clear;
            }

            // update the text
            ProgressText.text = string.Format("{0}/{1}", currencrystals.ToString(), totalcrystals.ToString());
        }


    }

    private int currencrystals;
    public int CurrentCrystals
    {
        get { return currencrystals; }
        set {
            if (value >= Crystals.Length)
                currencrystals = Crystals.Length;
            else if (value < 0)
                currencrystals = 0;
            else
                currencrystals = value;


            for (int i = 0; i < totalcrystals; i++)
            {
                if (i < currencrystals)
                    Crystals[i].color = Color.white;
                else
                    Crystals[i].color = Color.gray;
            }

            // update the text
            ProgressText.text = string.Format("{0}/{1}", currencrystals.ToString(), totalcrystals.ToString());

        }
    }
}
