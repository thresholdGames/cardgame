﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PreviewManager : UnityEngine.MonoBehaviour {

    public static PreviewManager instance;
    public bool canPreview = true;
    public HoverPreview preview;

    private void Awake()
    {
        if(!instance)
        {
            instance = this;
        }
    }

    public void StopAllPreviews()
    {
        preview.StopThisPreview();
    }

    private void Update()
    {
        if(CommandManager.instance.commandPlaying == null)
        {
            canPreview = true;
        }
        else
        {
            canPreview = false;
        }
    }
}
