﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CreatureCardVisual : CardVisual {

    public Text HealthText;
    public Text AttackText;

    [Header("Data")]
    public CreatureCardAsset CardAsset;

    public override CardAsset asset
    {
        get
        {
            return CardAsset;
        }
        set
        {
            CardAsset = value as CreatureCardAsset;
        }
    }

    public override void LoadAssets()
    {
        base.LoadAssets();
        CardBody.color = CardAsset.type.ClassCardTint;
        CardName.color = CardAsset.type.ClassRibbonsTint;
        CardType.color = CardAsset.type.ClassRibbonsTint;
        HealthText.text = CardAsset.Health.ToString();
        AttackText.text = CardAsset.Attack.ToString();
    }

    private void Update()
    {
        
    }
}
