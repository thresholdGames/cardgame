﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MessageManager : UnityEngine.MonoBehaviour {

    public static MessageManager instance;
    public GameObject panel;
    public Text message;

    private void Awake()
    {
        if(!instance)
        {
            instance = this;
        }
    }

    public void PlayerTurn(PlayerLogic player)
    {
        panel.SetActive(true);
        if (player == GameManager.Instance.currentPlayer)
        {
            message.text = "Your Turn";
        }

        if (player == GameManager.Instance.otherPlayer)
        {
            message.text = "Enemy Turn";
        }
        StartCoroutine(KillPanel());

    }

    public void PlayerWins(PlayerLogic player)
    {
        panel.SetActive(true);
        if(player == GameManager.Instance.currentPlayer)
        {
            message.text = "Victory";
        }

        if (player == GameManager.Instance.otherPlayer)
        {
            message.text = "Defeat";
        }
    }

    IEnumerator KillPanel()
    {
        yield return new WaitForSeconds(1.5f);
        panel.SetActive(false);
    }
}
