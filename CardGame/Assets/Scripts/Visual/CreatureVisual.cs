﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CreatureVisual : UnityEngine.MonoBehaviour {

    [Header("Logic")]
    public CreatureLogic logic;

    [Header("Data")]
    public CreatureCardAsset asset;
    public CreatureCardVisual preview;

    [Header("Images")]
    public Image CreatureImage;
    public Image CreatureGlow;

    [Header("Text Images")]
    public Text AttackText;
    public Text HealthText;

    [Header("Target")]
    public GameObject target;

    [Header("Spark")]
    public GameObject spark;

    //void Start ()
    //{
    //    if (asset)
    //        LoadAssets();
    //}

    public void LoadAssets()
    {
        CreatureImage.sprite = asset.CardImage;
        AttackText.text = asset.Attack.ToString();
        HealthText.text = asset.Health.ToString();
        if(preview)
        {
            preview.CardAsset = asset;
            preview.LoadAssets();
        }

    }

    public void UpdateGlow()
    {
        if (logic)
        {
            CreatureGlow.enabled = logic.CanAttack;
            spark.SetActive(logic.CanActivateSkill);
        }
    }
    public virtual void Update()
    {
        if (logic)
        {
            if (logic.Health < logic.NormalHealth)
                HealthText.color = Color.red;
            else
                HealthText.color = Color.white;
        }
    }

}
