﻿using UnityEngine;
using System.Collections;

public enum AreaPosition{Top, Low}

public class PlayerArea : UnityEngine.MonoBehaviour 
{
    public AreaPosition owner;
    public bool ControlsON = true;
    public HandVisual handVisual;
    public FieldVisual fieldVisual;
    public PoolVisual AnimaVisual;
    public PoolVisual EssenceVisual;
    public DeckVisual deckVisual;
    //public GraveyardVisual graveyardVisual;
    public PortraitVisual portraitVisual;

    /*
    public PlayerDeckVisual PDeck;
    public ManaPoolVisual ManaBar;
    public HandVisual handVisual;
    public PlayerPortraitVisual Portrait;
    public HeroPowerButton HeroPower;
    public EndTurnButton EndTurnButton;
    public TableVisual tableVisual;
    public Transform PortraitPosition;
    public Transform InitialPortraitPosition;
    */

    public bool AllowedToControlThisPlayer
    {
        get;
        set;
    }      


}
