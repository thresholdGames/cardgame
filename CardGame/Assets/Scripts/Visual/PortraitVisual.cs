﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PortraitVisual : UnityEngine.MonoBehaviour {

    public PlayerLogic player;
    public Text HealthText;
    private int health;
    public int Health
    {
        get { return health; }
        set { health = value;
            HealthText.text = health.ToString();
        }
    }

    private void Start()
    {
        HealthText.text = player.MaxHealth.ToString();
        health = player.MaxHealth;
    }

}
