﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;


public class HandVisual : UnityEngine.MonoBehaviour {

    private List<GameObject> cardsInHand = new List<GameObject>();
    public SameDistanceChildren slots;
    public Transform DrawSpot;

    public void AddCard(CardAsset ca, PlayerLogic player)
    {
        GameObject card = CreateCard(ca,player);
        cardsInHand.Add(card);
        foreach (GameObject g in cardsInHand)
        {
            if (g.transform.position != slots.Children[cardsInHand.IndexOf(g)].position)
            {
                g.transform.DOMove(slots.Children[cardsInHand.IndexOf(g)].position, 0.5f);
            }
        }

        CommandManager.instance.SkipCommand();
    }

    public void RemoveCard(GameObject card)
    {
        cardsInHand.Remove(card);
        foreach (GameObject g in cardsInHand)
        {
            if (g.transform.position != slots.Children[cardsInHand.IndexOf(g)].position)
            {
                g.transform.DOMove(slots.Children[cardsInHand.IndexOf(g)].position, 0.5f);
            }
        }
    }

    GameObject CreateCard(CardAsset card, PlayerLogic player)
    {
        CardVisual cv = null;
        GameObject c = null;
        CardLogic cl = null;
        if (card.CardType == CardType.Creature)
        {
            c = PoolManager.instance.GetCreatureCard();
            cv = c.GetComponent<CreatureCardVisual>();
            cl = c.GetComponent<CreatureCardLogic>();
        }
        if (card.CardType == CardType.Spell)
        {
            c = PoolManager.instance.GetSpellCard();
            cv = c.GetComponent<SpellCardVisual>();
            cl = c.GetComponent<SpellCardLogic>();
        }

       
        
        cl.Load(card, player);
        cv.asset = card;
        cv.LoadAssets();
        player.hand.hand.Add(cl);
        c.SetActive(true);
        c.transform.position = DrawSpot.position;

        return c;   
    }
}