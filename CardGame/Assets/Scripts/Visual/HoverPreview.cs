﻿using UnityEngine;
using System.Collections;
using DG.Tweening;

public class HoverPreview: UnityEngine.MonoBehaviour
{
    public GameObject TurnThisOffWhenPreviewing;
    public Vector3 TargetPosition;
    public float TargetScale;
    public GameObject previewGameObject;
 
 
    // MONOBEHVIOUR METHODS
    void Awake()
    {
    }

    void OnMouseEnter()
    {
        if (PreviewManager.instance.canPreview)
            PreviewThis();
    }

    void OnMouseExit()
    {
        StopThisPreview();
    }

    public void PreviewThis()
    {
        
        PreviewManager.instance.preview = this;
        previewGameObject.SetActive(true);
        if(TurnThisOffWhenPreviewing)
            TurnThisOffWhenPreviewing.SetActive(false);
        previewGameObject.transform.localPosition = Vector3.zero;
        previewGameObject.transform.localScale = Vector3.one;
        previewGameObject.transform.DOLocalMove(TargetPosition, 0.1f);
        previewGameObject.transform.DOScale(TargetScale, 0.1f);
    }
   
    public void StopThisPreview()
    {   
        previewGameObject.SetActive(false);
        previewGameObject.transform.localPosition = Vector3.zero;
        previewGameObject.transform.localScale = Vector3.one;
        if (TurnThisOffWhenPreviewing)
            TurnThisOffWhenPreviewing.SetActive(true);
    }
}
