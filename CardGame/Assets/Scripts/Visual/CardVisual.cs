﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public abstract class CardVisual : UnityEngine.MonoBehaviour {

    abstract public CardAsset asset
    {
        get;
        set;
    }

    [Header("Logic")]
    public CardLogic cardLogic;

    [Header("Preview")]
    public CardVisual preview;

    [Header("Card Parts")]
    public Image CardName;
    public Image CardType;
    public Image CardImage;
    public Image CardBody;
    public Image CardFrame;
    public Image CardGlow;

    [Header("Card Text")]
    public Text NameText;
    public Text ManaCostText;
    public Text DescriptionText;

   public virtual void LoadAssets()
    {
        NameText.text = asset.CardName;
        ManaCostText.text = asset.ManaCost.ToString();
        DescriptionText.text = asset.Description;
        CardImage.sprite = asset.CardImage;
        if (preview != null )
        {
            preview.asset = asset;
            preview.LoadAssets();
        }
        CardGlow.enabled = false;
    }

    public void UpdateGlow()
    {
        if (cardLogic)
            CardGlow.enabled = cardLogic.CanBePlayed;
    }
}
