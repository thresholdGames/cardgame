﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DraggableCreature : Draggable {

    public LayerMask portraitMask;  
    public LayerMask creatureMask;
    private CreatureLogic creature;
    public bool hasEffect = false;
    public bool pressing = false;

    public override void Start()
    {
        base.Start();
        creature = GetComponent<CreatureLogic>();
    }

    public override void OnDragStart()
    {
        if (creature.CanAttack)
        {
            PreviewManager.instance.StopAllPreviews();
            PreviewManager.instance.canPreview = false;
            targetSR.enabled = true;
            line.enabled = true;
        }
    }

    public override void OnDragging()
    {
        if (creature.CanAttack)
        {
            Vector3 distance = target.transform.position - transform.position;
            Vector3 direction = distance.normalized;
            Vector3 mousePos = Input.mousePosition;
            mousePos.z = offset;
            mousePos = Camera.main.ScreenToWorldPoint(mousePos);
            target.transform.position = mousePos;
            line.SetPositions(new Vector3[] { gameObject.transform.position, mousePos - direction * 1.5f });
        }
    }

    public override void OnDragEnd()
    {
        if (creature.CanAttack)
        {
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);  
            RaycastHit hit;

            if (Physics.Raycast(ray, out hit, Mathf.Infinity, creatureMask))
            {
                CreatureLogic creatureTarget = hit.collider.GetComponent<CreatureLogic>();
                if (creatureTarget.owner != creature.owner && creatureTarget.Health > 0)
                    creature.Attack(creatureTarget);
            }

            if (Physics.Raycast(ray, out hit, Mathf.Infinity, portraitMask))
            {
                PortraitVisual portraitTarget = hit.collider.GetComponent<PortraitVisual>();
                if (portraitTarget.player != creature.owner && portraitTarget.player.field.isEmpty())
                    creature.Attack(portraitTarget.player);
            }

            line.enabled = false;
            targetSR.enabled = false; 
            target.transform.position = transform.position;
            line.SetPositions(new Vector3[] { transform.position, transform.position});
            PreviewManager.instance.canPreview = true;

        }
    }
}
