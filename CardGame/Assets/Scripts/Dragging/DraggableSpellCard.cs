﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class DraggableSpellCard : Draggable {

    public LayerMask fieldMask;
    SpellCardLogic card;

    public override void Start()
    {
        card = GetComponent<SpellCardLogic>();
    }

    public override void OnDragEnd()
    {
        Sequence s = DOTween.Sequence();
        PreviewManager.instance.canPreview = true;
        if (card.CanBePlayed)
        {
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            RaycastHit hit;

            if (Physics.Raycast(ray, out hit, Mathf.Infinity, fieldMask))
            {
                card.owner.PlaySpell(card, gameObject);
            }
            else
            {
                s.Append(transform.DOMove(startPos, 0.5f));
            }
        }

        else
        {
            s.Append(transform.DOMove(startPos, 0.5f));
        }
        returning = true;
        s.OnComplete(() => returning = false);
    }

    public override void OnDragging()
    {
        Vector3 mousePos = Input.mousePosition;
        mousePos.z = offset;
        mousePos = Camera.main.ScreenToWorldPoint(mousePos);
        transform.position = mousePos;
    }

    public override void OnDragStart()
    {
        startPos = transform.position;
        PreviewManager.instance.StopAllPreviews();
        PreviewManager.instance.canPreview = false;
    }

}
