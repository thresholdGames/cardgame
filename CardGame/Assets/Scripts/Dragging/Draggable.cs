﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class Draggable : UnityEngine.MonoBehaviour
{
    public Vector3 startPos;
    public float offset = 17f;
    public GameObject target;
    protected LineRenderer line;
    protected SpriteRenderer triangle;
    protected SpriteRenderer targetSR;
    public bool returning = false;
    public bool CanDrag
    {
        get
        {
            bool command = CommandManager.instance.commandPlaying == null;
            return command & !returning;
        }
    }

    public virtual void Start()
    {
        line = target.GetComponentInChildren<LineRenderer>();
        targetSR = target.GetComponent<SpriteRenderer>();
        triangle = target.transform.Find("Triangle").GetComponent<SpriteRenderer>();
    }

    public virtual void OnDragStart() { }
    public virtual void OnDragEnd() { }
    public virtual void OnDragging() { }

    public void OnMouseDown()
    {
        OnDragStart();
    }

    public void OnMouseDrag()
    {
        if (CanDrag)
        {
            PreviewManager.instance.canPreview = false;
            OnDragging();
        }
    }

    public void OnMouseUp()
    {   
        if(CanDrag)
            OnDragEnd();
    }
}