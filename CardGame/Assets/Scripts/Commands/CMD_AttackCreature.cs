﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class CMD_AttackCreature : Command {

    CreatureLogic attacker;
    CreatureLogic target;
    bool running = false;

    public CMD_AttackCreature(CreatureLogic a, CreatureLogic t)
    {
        attacker = a;
        target = t;
    }

    public override CMDState Execute()
    {
        if (!running)
        {
            Sequence s = DOTween.Sequence();
            s.Append(attacker.transform.DOMove(target.transform.position,0.1f));
            s.AppendCallback(Attack);
            s.Append(attacker.transform.DOMove(attacker.transform.position, 0.1f));
            s.OnComplete(() => CommandManager.instance.SkipCommand());
            running = true;
        }     
        return CMDState.Executing;
    }

    public void Attack()
    {
        target.GetHit(attacker.CurrentAttack);
        if (target.Health > 0)
        {
            attacker.Retaliation(target.CurrentAttack);
        }
        attacker.AttacksLeft -= 1;
    }
}
