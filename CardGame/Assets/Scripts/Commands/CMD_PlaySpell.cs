﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class CMD_PlaySpell : Command {

    PlayerLogic player;
    GameObject cardGO;
    SpellCardLogic spell;

    public CMD_PlaySpell(PlayerLogic p,SpellCardLogic s,GameObject cgo)
    {
        player = p;
        cardGO = cgo;
        spell = s;

    }

    public override CMDState Execute()
    {
        player.PArea.handVisual.RemoveCard(cardGO);
        player.hand.hand.Remove(spell);
        cardGO.SetActive(false);
        cardGO.transform.DOScale(1.5f, 0.1f);
        spell.ActivateEffect();
        return CMDState.Done;
    }

}
