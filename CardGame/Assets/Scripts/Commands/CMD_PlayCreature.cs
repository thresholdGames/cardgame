﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class CMD_PlayCreature : Command {


    bool running = false;
    PlayerLogic player;
    CreatureCardLogic card;

    public CMD_PlayCreature(PlayerLogic p, CreatureCardLogic c)
    {
        player = p;
        card = c;
    }

    public override CMDState Execute()
    {
        if (!running)
        {
            Sequence s = DOTween.Sequence();
            s.Append(card.gameObject.transform.DOMove(GlobalSettings.instance.PlayPreview.position,0.2f));
            s.Append(card.gameObject.transform.DOScale(4f, 0.5f));
            player.PArea.handVisual.RemoveCard(card.gameObject);
            player.hand.hand.Remove(card);
            running = true;
            s.OnComplete(PlayCreature);
        }
        return CMDState.Executing;
    }

    void PlayCreature()
    {
        card.gameObject.SetActive(false);
        card.gameObject.transform.DOScale(1.5f, 0.1f);
        player.PArea.fieldVisual.AddCard(card, player);
        
    }
}
