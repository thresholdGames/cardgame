﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CMD_Revive : Command {

    PlayerLogic player;

    public CMD_Revive(PlayerLogic p)
    {
        player = p;
    }

    public override CMDState Execute()
    {
        foreach (CardAsset card in player.graveyard.graveyard)
        {
            if (TargetManager.instance.shownTargets.Contains(card.GetInstanceID()))
            {
                player.ReviveACard(card);
            }
        }
        return CMDState.Done;
    }
}
