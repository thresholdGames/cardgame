﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class CMD_AttackFace : Command
{

    CreatureLogic attacker;
    PlayerLogic target;
    bool running = false;

    public CMD_AttackFace(CreatureLogic a, PlayerLogic t)
    {
        attacker = a;
        target = t;
    }

    public override CMDState Execute()
    {
        if (!running)
        {
            Sequence s = DOTween.Sequence();
            s.Append(attacker.transform.DOMove(target.transform.position, 0.1f));
            s.AppendCallback(Attack);
            s.Append(attacker.transform.DOMove(attacker.transform.position, 0.1f));
            s.OnComplete(() => CommandManager.instance.SkipCommand());
            running = true;
        }

        return CMDState.Executing;
    }

    public void Attack()
    {
        target.GetHit(attacker.CurrentAttack);
        attacker.AttacksLeft -= 1;
    }
}
