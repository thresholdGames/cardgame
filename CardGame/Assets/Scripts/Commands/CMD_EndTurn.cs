﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CMD_EndTurn : Command {

    PlayerLogic turnPlayer;

    public CMD_EndTurn(PlayerLogic player)
    {
        turnPlayer = player;
    }

    public override CMDState Execute()
    {
        PlayerLogic aux = TurnManager.Instance.OtherPlayerTurn;
        TurnManager.Instance.OtherPlayerTurn = turnPlayer;
        TurnManager.Instance.CurrentTurnPlayer = aux;
        return CMDState.Done;
    }
}
