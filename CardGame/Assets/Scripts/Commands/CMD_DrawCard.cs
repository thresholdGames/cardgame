﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class CMD_DrawCard : Command {

    PlayerLogic player;
    bool running = false;

    public CMD_DrawCard(PlayerLogic owner)
    {
        player = owner;
    }

    public override CMDState Execute()
    {
        if (!running && player.deck.deck.Count > 0)
        {
            CardAsset card = player.deck.getFirstCard();
            player.deck.deck.RemoveAt(0);
            player.PArea.handVisual.AddCard(card,player);
            running = true;
        }
        else
        {
            return CMDState.Done;
        }
        return CMDState.Executing;
    }
}
