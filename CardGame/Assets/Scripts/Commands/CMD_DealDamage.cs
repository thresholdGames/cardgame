﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public class CMD_DealDamage : Command {
    CreatureLogic target = null;
    int damage;

    public CMD_DealDamage(int d) : base() { 
        damage = d;
    }

    public override CMDState Execute()
    {
        List<CreatureLogic> field = GameManager.Instance.currentPlayer.field.field.Concat(GameManager.Instance.otherPlayer.field.field).ToList();

        foreach (int targetID in TargetManager.instance.shownTargets)
        {
            foreach (CreatureLogic creature in field)
            {
                if (creature.ID == targetID)
                    target = creature;
            }

            if (target)
            {
                target.GetHit(damage);
            }
        }
        return CMDState.Done;
    }

}
