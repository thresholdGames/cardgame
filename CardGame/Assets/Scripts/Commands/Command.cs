﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Command
{

    public Command()
    {
        CommandManager.instance.AddToQueue(this);
    }

    public virtual CMDState Execute()
    {
        return CMDState.Done;
    }
}
