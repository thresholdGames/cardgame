﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class CMD_DiscardCard : Command {

    CardLogic target;
    PlayerLogic player;
    bool running = false;

    public CMD_DiscardCard(PlayerLogic p) : base()
    {
        player = p;
    }

    public override CMDState Execute()
    {
        if (!running)
        {
            foreach (int targetID in TargetManager.instance.shownTargets)
            {
                foreach (CardLogic card in player.hand.hand)
                {
                    if (card.ID == targetID)
                        target = card;
                }

                if (target)
                {
                    Sequence s = DOTween.Sequence();
                    player.PArea.handVisual.RemoveCard(target.gameObject);
                    player.hand.hand.Remove(target);
                    player.graveyard.AddCard(target.asset);
                    s.Append(target.transform.DOMoveY(0.00001f, 1));
                    s.OnComplete(Discard);
                }
            }
            running = true;
        }
        return CMDState.Executing;
    }

    void Discard()
    {
        target.gameObject.SetActive(false);
        CommandManager.instance.SkipCommand();
    }
}
