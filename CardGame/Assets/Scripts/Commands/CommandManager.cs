﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum CMDState
{
    None,
    Executing,
    Done,
    Failed
}

public class CommandManager : UnityEngine.MonoBehaviour
{

    public Queue<Command> CommandQueue = new Queue<Command>();
    public static CommandManager instance;
    public Command commandPlaying;

    public void Awake()
    {
        if (!instance)
            instance = this;
    }

    public void Update()
    {
        if (commandPlaying != null)
        {
            CMDState result = commandPlaying.Execute();
            if (result == CMDState.Executing)
                return;

            if (result == CMDState.Done)
            {
                PlayNextCommand();
            }
        }
    }

    public virtual void AddToQueue(Command cmd)
    {
        CommandQueue.Enqueue(cmd);
        if (commandPlaying == null)
            PlayNextCommand();
    }

    public void PlayNextCommand()
    {
        if (CommandQueue.Count > 0)
            commandPlaying = CommandQueue.Dequeue();

        else
        {
            commandPlaying = null;
            foreach (CardLogic c in TurnManager.Instance.CurrentTurnPlayer.hand.hand)
            {
                c.visual.UpdateGlow();
            }

            foreach (CreatureLogic c in TurnManager.Instance.CurrentTurnPlayer.field.field)
            {
                c.visual.UpdateGlow();
            }
        }
    }

    public void SkipCommand()
    {
        PlayNextCommand();
    }
}

