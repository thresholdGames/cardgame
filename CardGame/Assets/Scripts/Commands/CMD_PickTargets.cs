﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CMD_PickTargets : Command {

    enum Target
    {
        None,
        CretureLogic,
        CardAsset,
        CardLogic
    }

    Target targetType;

    List<CreatureLogic> t_CreatureL = null;
    List<CardAsset> t_CardA = null;
    List<CardLogic> t_CardL = null;
    int quantityTargets = 0;
    bool Executing = false;
    string text = "";

    public CMD_PickTargets(List<CreatureLogic> targets, int qt,string t) : base()
    {
        targetType = Target.CretureLogic;
        t_CreatureL = targets;
        quantityTargets = qt;
        text = t;
    }

    public CMD_PickTargets(List<CardAsset> targets, int qt, string t)
    {
        targetType = Target.CardAsset;
        t_CardA = targets;
        quantityTargets = qt;
        text = t;
    }

    public CMD_PickTargets(List<CardLogic> targets, int qt, string t) : base()
    {
        targetType = Target.CardLogic;
        t_CardL = targets;
        quantityTargets = qt;
        text = t;
    }


    public override CMDState Execute () {

        if(!Executing)
        {
            Executing = true;
            Picktarget();
        }

        if (TargetManager.instance.getTargets(quantityTargets))
        {
            return CMDState.Done;
        }

        return CMDState.Executing;
	}

    public void Picktarget()
    {
        switch (targetType)
        {
            case Target.CretureLogic:
                TargetManager.instance.ShowTargets(t_CreatureL,text);
                break;
            case Target.CardAsset:
                TargetManager.instance.ShowTargets(t_CardA, text);
                break;
            case Target.CardLogic:
                TargetManager.instance.ShowTargets(t_CardL, text);
                break;
            default:
                break;
        }
    }

}
