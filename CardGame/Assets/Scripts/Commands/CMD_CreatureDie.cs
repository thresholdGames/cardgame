﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CMD_CreatureDie : Command {


    CreatureLogic card = null;

    public CMD_CreatureDie(CreatureLogic cl)
    {
        card = cl;
    }

    public override CMDState Execute()
    {
        card.gameObject.SetActive(false);
        card.owner.field.field.Remove(card);
        card.owner.PArea.fieldVisual.RemoveCard(card.gameObject);
        card.DeathWish();
        card.owner.graveyard.AddCard(card.visual.asset);
        return CMDState.Done;
    }

}
