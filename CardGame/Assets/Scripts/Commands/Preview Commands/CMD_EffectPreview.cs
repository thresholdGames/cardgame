﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class CMD_EffectPreview : Command {

    float delay = 2.0f;
    CMDState state = CMDState.Executing;

    public CMD_EffectPreview(CardAsset card):base()
    {
        TargetManager.instance.EffectPreview(card, delay);
        Sequence s = DOTween.Sequence();
        s.PrependInterval(delay);
        s.OnComplete(Done);      
    }

    public override CMDState Execute()
    {
        return state;
    }

    void Done()
    {
        state = CMDState.Done;
    }
}
