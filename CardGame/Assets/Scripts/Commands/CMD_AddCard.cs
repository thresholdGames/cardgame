﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CMD_AddCard : Command{

    PlayerLogic player;

    public CMD_AddCard(PlayerLogic p)
    {
        player = p; 
    }

    public override CMDState Execute()
    {
        foreach (CardAsset card in player.deck.deck)
        {
            if (TargetManager.instance.shownTargets.Contains(card.GetInstanceID()))
            {
                player.AddCardFromDeck(card);
                TargetManager.instance.shownTargets.Clear();
                return CMDState.Done;
            }
        }
        return CMDState.Done;
    }
}
