﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TargetLogic : UnityEngine.MonoBehaviour {

    public int id;
    int auxID = -1;


    public void ClickDown()
    {
        auxID = id;
    }

    public void ClickUp()
    {
        if (id == auxID)
        {
            TargetManager.instance.shownTargets.Add(id);
            gameObject.SetActive(false);
        }
    }

    public void ClickExit()
    {
        auxID = -1;
    }
}
