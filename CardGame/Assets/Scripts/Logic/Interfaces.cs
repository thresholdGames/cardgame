﻿using UnityEngine;
using System.Collections;

public interface ICharacter: IIdentifiable
{	
    int Health { get;    set;}

    void Die();

    void GetHit(int damage);
}

public interface IIdentifiable
{
    int ID { get; }
}


