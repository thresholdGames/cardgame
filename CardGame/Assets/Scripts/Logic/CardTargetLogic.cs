﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CardTargetLogic : UnityEngine.MonoBehaviour {

    public CardLogic card;

    public void OnMouseDown()
    {
        TargetManager.instance.shownTargets.Add(card.ID);
        gameObject.SetActive(false);
    }
}
