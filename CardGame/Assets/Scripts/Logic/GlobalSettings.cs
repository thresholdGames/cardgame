﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GlobalSettings : UnityEngine.MonoBehaviour {

    public GameObject CreatureCard;
    public GameObject SpellCard;
    public GameObject Creature;
    public static GlobalSettings instance;
    public Transform DrawPreview;
    public Transform PlayPreview;

    private void Awake()
    {
        if (!instance)
            instance = this;
    }
}
