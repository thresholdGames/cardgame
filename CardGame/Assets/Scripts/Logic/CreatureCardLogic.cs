﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CreatureCardLogic : CardLogic {

    private int attack;
    private int health;
    public CreatureCardAsset cardAsset;
    public CreatureEffect effect;
    public override bool CanBePlayed
    {
        get
        {
            if(effect.ActionCondition())
            {
                visual.CardGlow.color = Color.yellow;
            }

            else
            {
                visual.CardGlow.color = Color.green;
            }
            
            bool PlayerTurn = TurnManager.Instance.MyTurn(owner);
            bool FieldNotFull = !owner.field.isFull();
            bool HaveAnima = owner.CurrentAnima >= currentManaCost;
            return (PlayerTurn & FieldNotFull & HaveAnima & GameManager.Instance.Playing);
        }
    }

    public override void Load(CardAsset ca, PlayerLogic player)
    {
        visual = GetComponent<CreatureCardVisual>();
        cardAsset = ca as CreatureCardAsset;
        base.Load(ca, player);
        attack = cardAsset.Attack;
        health = cardAsset.Health;
        try
        {
            effect = System.Activator.CreateInstance(System.Type.GetType(cardAsset.name), new System.Object[] { this }) as CreatureEffect;
        }
        catch
        {
            effect = new NoEffect(this);
        }
    }
}
