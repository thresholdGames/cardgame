﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FieldLogic : UnityEngine.MonoBehaviour
{

    public List<CreatureLogic> field = new List<CreatureLogic>();

    [SerializeField]
    private int maxCardsInField = 5;

    public void PlaceCreature(int index, CreatureLogic card)
    {
        field.Insert(index, card);
    }

    public bool isFull()
    {
        return field.Count >= maxCardsInField;
    }

    public bool isEmpty()
    {
        return field.Count == 0;
    }
}
