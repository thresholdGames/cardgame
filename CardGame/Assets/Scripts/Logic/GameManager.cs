﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class GameManager : UnityEngine.MonoBehaviour {

    public static GameManager Instance;

    private bool playing = true;
    public bool Playing
    {
        get
        {
            return playing;
        }
        set
        {
            playing = value;
        }
    }

    [Header("Players")]
    public PlayerLogic currentPlayer;
    public PlayerLogic otherPlayer;

    [Header("Game Vars")]
    public int initDraw = 0;
    private PlayerLogic First;
    private PlayerLogic Second;


    private void Awake()
    {
        if(!Instance)
        {
            Instance = this;
        }
    }

    private void Start()
    {
       currentPlayer.enemy = otherPlayer;
       otherPlayer.enemy = currentPlayer;
       OnGameStart();
    }

    public void OnGameStart()
    {
        currentPlayer.OnGameStart();
        otherPlayer.OnGameStart();
        ChooseFirst();
        for (int i = 0; i < initDraw; i++)
        {
            First.DrawCard();
            Second.DrawCard();
        }
        Second.DrawCard();
        TurnManager.Instance.CurrentTurnPlayer = First;
        TurnManager.Instance.OtherPlayerTurn = Second;
        //new StartATurnCommand(First).AddToQueue();
                
        // move both portraits to the center
        //p.PArea.Portrait.transform.position = p.PArea.handVisual.OtherCardDrawSourceTransform.position;
        /*
        Sequence s = DOTween.Sequence();
        
        s.Append(Player.Players[0].PArea.Portrait.transform.DOMove(Player.Players[0].PArea.PortraitPosition.position, 1f).SetEase(Ease.InQuad));
        s.Insert(0f, Player.Players[1].PArea.Portrait.transform.DOMove(Player.Players[1].PArea.PortraitPosition.position, 1f).SetEase(Ease.InQuad));
        
        s.PrependInterval(3f);
        s.OnComplete(() =>
        {
            // determine who starts the game.
            int rnd = Random.Range(0, 2);  // 2 is exclusive boundary
                                           // Debug.Log(Player.Players.Length);
            Player whoGoesFirst = Player.Players[rnd];
            // Debug.Log(whoGoesFirst);
            Player whoGoesSecond = whoGoesFirst.otherPlayer;
            // Debug.Log(whoGoesSecond);

            // draw 4 cards for first player and 5 for second player
            int initDraw = 4;
            for (int i = 0; i < initDraw; i++)
            {
                // second player draws a card
                whoGoesSecond.DrawACard(true);
                // first player draws a card
                whoGoesFirst.DrawACard(true);
            }
            // add one more card to second player`s hand
            whoGoesSecond.DrawACard(true);

            //new GivePlayerACoinCommand(null, whoGoesSecond).AddToQueue();
            //whoGoesSecond.DrawACoin();
            
        });*/
    }

    private void ChooseFirst()
    {
        int rng = Random.Range(0,2);
        switch (rng)
        {
            case 0:
                First = currentPlayer;
                Second = otherPlayer;
                break;
            case 1:
                First = otherPlayer;
                Second = currentPlayer;
                break;
            default:
                break;
        }
    }

    public void PlayerWins(PlayerLogic player)
    {
        Playing = false;
        MessageManager.instance.PlayerWins(player);
    }

}
