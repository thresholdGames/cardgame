﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using UnityEngine;

public class PlayerLogic : UnityEngine.MonoBehaviour, ICharacter
{
    public PlayerArea PArea;
    public DeckLogic deck;
    public HandLogic hand;
    public FieldLogic field;
    public GraveyardLogic graveyard;
    public PlayerLogic enemy;

    private int playerID;
    public int ID
    { get { return playerID; } }
    private int health;
   public int Health
    {
        get{ return health; }
        set{ health = value;
            PArea.portraitVisual.Health = health;
            if (health <= 0)
                Die();
        }
    }
    public int maxHealth;
    public int MaxHealth
    {
        get { return maxHealth; }
        set { maxHealth = value; }
    }

    // TODO GAIN RESOURCES
    //Anima
    public int maxAnima;
    private int currentmaxAnima;
    public int CurrentMaxAnima
    {
        get { return currentmaxAnima; }
        set {
            if (value > maxAnima)
            {
                currentmaxAnima = maxAnima;
            }
            else if (value <= 0)
            {
                currentmaxAnima = 0;
            }
            else
            {
                currentmaxAnima = value;
            }
            PArea.AnimaVisual.TotalCrystals = currentmaxAnima;
        }
    }
    private int currentAnima;
    public int CurrentAnima
    {
        get { return currentAnima; }
        set {
            if (value > CurrentMaxAnima)
            {
                currentAnima = CurrentMaxAnima;
            }
            else if(value <= 0)
            {
                currentAnima = 0;
            }
            else
            {
                currentAnima = value;
            }
            PArea.AnimaVisual.CurrentCrystals = currentAnima;
        }
    }

    //Essence
    public int maxEssence;
    private int currentmaxEssence;
    public int CurrentMaxEssence
    {
        get { return currentmaxEssence; }
        set
        {
            if (value > maxEssence)
            {
                currentmaxEssence = maxEssence;
            }
            else if (value <= 0)
            {
                currentmaxEssence = 0;
            }
            else
            {
                currentmaxEssence = value;
            }
            PArea.EssenceVisual.TotalCrystals = currentmaxEssence;
        }
    }
    private int currentEssence;
    public int CurrentEssence
    {
        get { return currentEssence; }
        set
        {
            if (value > CurrentMaxEssence)
            {
                currentEssence = CurrentMaxEssence;
            }
            else if (value <= 0)
            {
                currentEssence = 0;
            }
            else
            {
                currentEssence = value;
            }
            PArea.EssenceVisual.CurrentCrystals = currentEssence;
        }
    }


    private void Awake()
    {
        playerID = IDFactory.GetUniqueID();
        Health = MaxHealth;
    }


    public void DrawCard()
    {
        if (deck.deck.Count > 0)
        {
            if (hand.getCardAmount() < hand.MaxCardsInHand)
            {
                new CMD_DrawCard(this);
                PArea.deckVisual.DrawCard();
                PArea.deckVisual.UpdateDeckCount(deck.deck.Count);
            }
        }
        else
        {
            //TODO WHAT HAPPENS WHEN NO CARDS LEFT
        }
    }

    public void AddCardFromDeck(CardAsset card)
    {
        deck.deck.Remove(card);
        PArea.handVisual.AddCard(card, this);
        PArea.deckVisual.DrawCard();
        PArea.deckVisual.UpdateDeckCount(deck.deck.Count);

    }
    
    public void PlayCreature(CreatureCardLogic card)
    {

        PArea.AnimaVisual.CurrentCrystals -= card.ManaCost;
        currentAnima -= card.ManaCost;
        new CMD_PlayCreature(this, card);

        foreach (CardLogic c in hand.hand)
        {
            c.visual.UpdateGlow();
        }

    }

    public void PlaySpell(SpellCardLogic card, GameObject cardGO)
    {
        PArea.EssenceVisual.CurrentCrystals -= card.ManaCost;
        currentEssence -= card.ManaCost;
        new CMD_PlaySpell(this, card ,cardGO);

        foreach (CardLogic c in hand.hand)
        {
            c.visual.UpdateGlow();
        }

    }


    public void DiscardCard(CardLogic card)
    {

    }

    public void ReviveACard(CardAsset card)
    {
        graveyard.graveyard.Remove(card);
        PArea.fieldVisual.AddCard(card, this);
    }

    public void GetHit(int damage)
    {
        Health -= damage;
    }

    public void Die()
    {
        GameManager.Instance.PlayerWins(enemy);
    }

    public void OnTurnStart()
    {
        MessageManager.instance.PlayerTurn(this);
        DrawCard();
        CurrentMaxAnima++;
        CurrentMaxEssence++;
        CurrentAnima = CurrentMaxAnima;
        CurrentEssence = CurrentMaxEssence;

        foreach(CardLogic card in hand.hand)
        {
            card.OnTurnStart();
        }

        foreach (CreatureLogic creature in field.field)
        {
            creature.OnTurnStart();
        }
    }

    public void OnTurnEnd()
    {
        foreach (CardLogic c in hand.hand)
        {
            c.visual.CardGlow.enabled = false;
        }

        foreach (CreatureLogic c in field.field)
        {
            c.visual.CreatureGlow.enabled = false;
            c.visual.spark.SetActive(false);
        }
    }

    public void OnGameStart()
    {
        currentmaxAnima = 0;
        currentmaxEssence = 0;
        currentAnima = 0;
        currentEssence = 0;
        //LoadCharacterInfoFromAsset();
        //TransmitInfoAboutPlayerToVisual();
        //PArea.PDeck.CardsInDeck = p.deck.cards.Count;
    }

    #region Testing
    /*
    private void OnGUI()
    {

        if (GUI.Button(new Rect(10, 10, 50, 20), "Draw"))
            DrawCard();

        if (GUI.Button(new Rect(10, 50, 50, 20), "+Anima"))
        {
            PArea.AnimaVisual.TotalCrystals++;
            PArea.AnimaVisual.CurrentCrystals++;
        }

        if (GUI.Button(new Rect(10, 90, 50, 20), "Reset"))
        {
            SceneManager.LoadScene(SceneManager.GetActiveScene().name);
        }

        if (GUI.Button(new Rect(10, 130, 50, 20), "Quit"))
        {
            Application.Quit();
        }
        
    }
*/
    #endregion
}
