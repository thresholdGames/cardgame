﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using DG.Tweening;

// this class will take care of switching turns and counting down time until the turn expires
public class TurnManager : UnityEngine.MonoBehaviour {

    //private RopeTimer timer;

    public static TurnManager Instance;
    private PlayerLogic currentTurnPlayer;
    public PlayerLogic CurrentTurnPlayer
    {
        get { return currentTurnPlayer; }
        set
        {
            currentTurnPlayer = value;
            currentTurnPlayer.OnTurnStart();
        }
    }
    private PlayerLogic otherTurnPlayer;
    public PlayerLogic OtherPlayerTurn
    {
        get { return otherTurnPlayer; }
        set
        {
            otherTurnPlayer = value;
            otherTurnPlayer.OnTurnEnd();
        }
    }

    void Awake()
    {
        if(!Instance)
        {
            Instance = this;
        }
        //timer = GetComponent<RopeTimer>();
    }

    public void EndTurn()
    {
        if(GameManager.Instance.Playing)
            new CMD_EndTurn(CurrentTurnPlayer);
        //timer.StopTimer();
    }

    public bool MyTurn(PlayerLogic player)
    {
        if (player == currentTurnPlayer)
            return true;
        else
            return false;
    }
}

