﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CreatureTargetLogic : UnityEngine.MonoBehaviour {


    public CreatureLogic card;

    public void OnMouseDown()
    {
        TargetManager.instance.shownTargets.Add(card.ID);
        gameObject.SetActive(false);
    }
}
