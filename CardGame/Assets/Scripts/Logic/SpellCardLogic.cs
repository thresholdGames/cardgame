﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpellCardLogic : CardLogic {

    public CardEffect effect;

    public override bool CanBePlayed
    {
        get
        {
            bool PlayerTurn = TurnManager.Instance.MyTurn(owner);
            bool HaveEssence = owner.CurrentEssence >= currentManaCost;
            bool CommandNotPlaying = CommandManager.instance.commandPlaying == null;
            return (PlayerTurn & HaveEssence & CommandNotPlaying & GameManager.Instance.Playing && effect.Condition());
        }
    }

    public override void Load(CardAsset ca, PlayerLogic player)
    {
        visual = GetComponent<SpellCardVisual>();
        SpellCardAsset c = ca as SpellCardAsset;
        base.Load(c, player);
        currentManaCost = c.ManaCost;
        try
        {
            effect = System.Activator.CreateInstance(System.Type.GetType(ca.name), new System.Object[] { this }) as CardEffect;
        }
        catch
        {
            Debug.Log("No effect found");
            effect = null;
            //effect = new NoEffect(this);
        }
    }

    public void ActivateEffect()
    {
        effect.Activation();
    }

}
