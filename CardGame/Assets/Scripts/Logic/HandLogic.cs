﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HandLogic : UnityEngine.MonoBehaviour {

    public List<CardLogic> hand = new List<CardLogic>();

    private int maxCardsInHand = 7;

    public int MaxCardsInHand
    {
        get { return maxCardsInHand; }
        set { maxCardsInHand = value;
        }
    }

    public void Shuffle()
    {
        hand.Shuffle();
    }

    public int getCardAmount()
    {
        return hand.Count;
    }

    public bool isFull()
    {
        return getCardAmount() == maxCardsInHand;           
    }
}
