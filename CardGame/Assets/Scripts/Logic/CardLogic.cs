﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CardLogic : UnityEngine.MonoBehaviour, IIdentifiable {

    public PlayerLogic owner;
    public CardAsset asset;
    public GameObject target;
    public CardVisual visual;

    protected string cardName;
    protected string currentName;
    protected int baseManaCost;
    protected int currentManaCost;
   
    
    public int ManaCost
    {
        get
        {
            return currentManaCost;
        }
        set
        {
            currentManaCost = value;
        }
    }

    protected int cardID;
    public int ID
    {
        get
        {
            return cardID;
        }
    }
    public string CardName
    {
        get
        {
            return currentName;
        }
        set
        {
            currentName = value;
        }
    }
    public virtual bool CanBePlayed { get { return false; } }

    public virtual void Load(CardAsset ca, PlayerLogic player)
    {
        cardID = IDFactory.GetUniqueID();
        asset = ca;
        owner = player;
        baseManaCost = ca.ManaCost;
        currentManaCost = baseManaCost;
    }

    public virtual void ResetMana()
    {
        currentManaCost = baseManaCost;
    }

    public void OnTurnStart()
    {
        //TODO 
    }

}
