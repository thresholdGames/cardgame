﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class CreatureLogic : UnityEngine.MonoBehaviour, ICharacter {

    public PlayerLogic owner;
    public CreatureVisual visual;
    public CreatureEffect effect = null;

    private int attack;

    public int CurrentAttack
    {
        get { return attack; }
        set {
            if (value < 0)
                attack = 0;
            else
            {
                attack = value;
            }
            visual.AttackText.text = attack.ToString();
        }
    }
    public int NormalAttack;
    private int health;
    public int Health
    {
        get { return health; }
        set { health = value;}
    }
    public int NormalHealth;
    private int attacksLeft = 1;
    public int AttacksLeft
    {
        get
        {
            return attacksLeft;
        }

        set
        {
            attacksLeft = value;
        }
    }

    private int skillLeft = 1;
    public int SkillLeft
    {
        get
        {
            return skillLeft;
        }

        set
        {
            skillLeft = value;
        }
    }

    private int creatureID;
    public int ID
    {
        get { return creatureID; }
    }
    public virtual bool CanAttack
    {   
        get
        {
            visual.CreatureGlow.color = Color.green;
            bool PlayerTurn = TurnManager.Instance.MyTurn(owner);
            bool HasAttacks = (attacksLeft > 0);
            bool HasAttackDamage = attack > 0;
            return (PlayerTurn && HasAttacks && HasAttackDamage && GameManager.Instance.Playing);
        }
    }

    public virtual bool CanActivateSkill
    {
        get
        {
            bool PlayerTurn = TurnManager.Instance.MyTurn(owner);
            bool HasSkills = (SkillLeft > 0);
            return (PlayerTurn && HasSkills && GameManager.Instance.Playing && effect.SkillCondition());
        }
    }


    public void Awake()
    {
        creatureID = IDFactory.GetUniqueID();
    }

    public void Load(CreatureVisual cv, PlayerLogic player,CreatureEffect eff)
    { 
        owner = player;
        visual = cv;
        NormalAttack = visual.asset.Attack;
        NormalHealth = visual.asset.Health; 
        CurrentAttack = NormalAttack;
        Health = NormalHealth;
        effect = eff;
        effect.creature = this;
        skillLeft = 1;
        attacksLeft = 1;
    }


    public void Attack(CreatureLogic target)
    {
        new CMD_AttackCreature(this, target);
    }

    public void Attack(PlayerLogic player)
    {
        new CMD_AttackFace(this,player);
    }

    public bool Damaged()
    {
        return (Health < NormalHealth);
    }

    public void Die()
    {
        new CMD_CreatureDie(this);
    }

    public void GetHit(int damage)
    {
        Health -= damage;
        visual.HealthText.text = health.ToString();
        gameObject.transform.DOShakePosition(1);
        if (health <= 0)
        {
            owner.GetHit(Mathf.Abs(health));
            Die();
        }
    }

    public void Retaliation(int damage)
    {
        Health -= damage;
        visual.HealthText.text = health.ToString();
        if (health <= 0)
        {
            owner.GetHit(Mathf.Abs(health));
            Die();
        }
    }

    public virtual void OnTurnStart()
    {
        attacksLeft = 1;
        skillLeft = 1;
    }

    public void Action()
    {
        if(effect.ActionCondition())
            effect.Action();
    }

    public void DeathWish()
    {
        effect.DeathWish();
    }

    public void Skill()
    {
        effect.Skill();
    }

    private void OnMouseOver()
    {
        if(Input.GetMouseButtonDown(1) && CanActivateSkill)
        {
            Skill();
        }
    }
}
