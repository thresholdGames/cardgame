﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public class GraveyardLogic : UnityEngine.MonoBehaviour {

    public List<CardAsset> graveyard = new List<CardAsset>();

    public CardAsset GetLastCard()
    {
        return graveyard.Last();
    }

    public void AddCard(CardAsset card)
    {
        graveyard.Add(card);
    }

    public void RemoveCard(CardAsset card)
    {
        graveyard.Remove(card);
    }

    public int GetAmount()
    {
        return graveyard.Count();
    }
}
