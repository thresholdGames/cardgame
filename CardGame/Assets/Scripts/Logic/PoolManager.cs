﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PoolManager : UnityEngine.MonoBehaviour {

    public static PoolManager instance;
    
    [Header("Cards")]
    public GameObject spellCard;
    public GameObject creatureCard;
    public int CreatureCardAmount;
    public int SpellCardAmount;
    List<GameObject> creatureCardList = new List<GameObject>();
    List<GameObject> spellCardList = new List<GameObject>();

    [Header("Creatures")]
    public GameObject creature;
    public int CreatureAmount;
    List<GameObject> creatureList = new List<GameObject>();

    public void Awake()
    {
        if(!instance)
        {
            instance = this;
        }
    }
    public void Start()
    {
        for (int i = 0; i < CreatureCardAmount; i++)
        {
            GameObject cc = Instantiate(creatureCard,gameObject.transform);
            cc.SetActive(false);
            creatureCardList.Add(cc);
        }

        for (int i = 0; i < SpellCardAmount; i++)
        {
            GameObject sc = Instantiate(spellCard, gameObject.transform);
            sc.SetActive(false);
            spellCardList.Add(sc);
        }

        for (int i = 0; i < CreatureAmount; i++)
        {
            GameObject c = Instantiate(creature, gameObject.transform);
            c.SetActive(false);
            creatureList.Add(c);
        }
    }

    public GameObject GetCreatureCard()
    {
        foreach(GameObject card in creatureCardList)
        {
            if (!card.activeInHierarchy)
                return card;
        }
        return null;
    }

    public GameObject GetCreature()
    {
        foreach (GameObject card in creatureList)
        {
            if (!card.activeInHierarchy)
                return card;
        }
        return null;
    }

    public GameObject GetSpellCard()
    {
        foreach (GameObject card in spellCardList)
        {
            if (!card.activeInHierarchy)
                return card;
        }
        return null;
    }
}
