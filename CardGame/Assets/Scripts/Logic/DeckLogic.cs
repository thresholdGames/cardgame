﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DeckLogic : UnityEngine.MonoBehaviour {

    public List<CardAsset> deck = new List<CardAsset>();

    private void Awake()
    {
        Shuffle();
    }

    public void Shuffle()
    {
        deck.Shuffle();
    }

    public CardAsset getFirstCard()
    {
        return deck[0];
    }

    public void  PlaceOnDeck(int index, CardAsset ca)
    {
        deck.Insert(index, ca);
    }
}
