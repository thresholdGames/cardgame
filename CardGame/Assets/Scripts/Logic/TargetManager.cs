﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;
public class TargetManager : UnityEngine.MonoBehaviour {



    public GameObject targetsPanel;
    public GameObject textPanel;
    public GameObject cards;
    public int quantity;
    public GameObject spellCardTargets;
    public GameObject creatureCardTargets;
    public GameObject spellCardTargetsEffect;
    public GameObject creatureCardTargetsEffect;
    public HorizontalLayoutGroup hlg;

    private List<GameObject> cardsList = new List<GameObject>();
    public static TargetManager instance = null;
    public List<int> shownTargets = new List<int>();

    public void Awake()
    {
        if (!instance)
        {
            instance = this;
        }
    }

    public void Start()
    {

        for (int i = 0; i < quantity; i++)
        {
            if (spellCardTargets)
            {
                GameObject spell = Instantiate(spellCardTargets) as GameObject;
                spell.transform.SetParent(cards.gameObject.transform, false);
                cardsList.Add(spell);
                spell.SetActive(false);
            }
        }

        for (int i = 0; i < quantity; i++)
        {
            if (creatureCardTargets)
            {
                GameObject creature = Instantiate(creatureCardTargets);
                creature.transform.SetParent(cards.gameObject.transform, false);
                cardsList.Add(creature);
                creature.SetActive(false);
            }
        }
    }

    public void ShowTargets(List<CardLogic> targets, string text)
    {
        ClearTargets();
        textPanel.GetComponentInChildren<Text>().text = text;
        for (int i = 0; i < targets.Count; i++)
        {
            targets[i].target.SetActive(true);
        }
    }

    public void ShowTargets(List<CreatureLogic> targets, string text)
    {
        ClearTargets();
        textPanel.SetActive(true);
        textPanel.GetComponentInChildren<Text>().text = text;

        for (int i = 0; i < targets.Count; i++)
        {
            targets[i].visual.target.SetActive(true);
        }
    }

    public void ShowTargets(List<CardAsset> targets, string text)
    {
        textPanel.SetActive(true);
        textPanel.GetComponentInChildren<Text>().text = text;
        PreviewManager.instance.canPreview = false;
        ClearTargets();
        targetsPanel.SetActive(true);
        for (int i = 0; i < targets.Count; i++)
        {
            if (targets[i].CardType == CardType.Creature)
            {
                GameObject creature = getPool(cardsList.FindAll(FindCreatures));
                CreatureCardVisual visual = creature.GetComponent<CreatureCardVisual>();
                TargetLogic targetLogic = creature.GetComponent<TargetLogic>();
                visual.asset = targets[i];
                visual.LoadAssets();
                targetLogic.id = targets[i].GetInstanceID();
                creature.SetActive(true);
            }

            if (targets[i].CardType == CardType.Spell)
            {
                GameObject spell = getPool(cardsList.FindAll(FindSpells));
                SpellCardVisual visual = spell.GetComponent<SpellCardVisual>();
                TargetLogic targetLogic = spell.GetComponent<TargetLogic>();
                visual.asset = targets[i];
                visual.LoadAssets();
                targetLogic.id = targets[i].GetInstanceID();
                spell.SetActive(true);
            }
        }

        hlg.transform.localPosition = new Vector3(9000, 0, 0);
    }

    GameObject getPool(List<GameObject> pool)
    {
        foreach (GameObject po in pool)
        {
            if (!po.activeInHierarchy)
                return po;
        }

        return null;
    }

    public bool getTargets(int quantity)
    {
        if (shownTargets.Count == quantity)
        {
            List<CreatureLogic> field = GameManager.Instance.currentPlayer.field.field.Concat(GameManager.Instance.otherPlayer.field.field).ToList();
            List<CardLogic> hand = GameManager.Instance.currentPlayer.hand.hand.Concat(GameManager.Instance.otherPlayer.hand.hand).ToList();
            targetsPanel.SetActive(false);
            textPanel.SetActive(false);
            PreviewManager.instance.canPreview = true;
            foreach (CreatureLogic c in field)
                c.visual.target.SetActive(false);

            foreach (CardLogic c in hand)
                c.target.SetActive(false);

            return true;
        }

        return false;
    }



    bool FindCreatures(GameObject go)
    {
        if (go.GetComponent<CreatureCardVisual>() != null)
        {
            return true;
        }

        return false;
    }

    bool FindSpells(GameObject go)
    {
        if (go.GetComponent<SpellCardVisual>() != null)
        {
            return true;
        }

        return false;
    }

    void ClearTargets()
    {
        TargetManager.instance.shownTargets.Clear();
        foreach (GameObject card in cardsList)
        {
            card.SetActive(false);
        }
    }

    public void EffectPreview(CardAsset effectAsset,float delay)
    {
        PreviewManager.instance.canPreview = false;
        CardVisual cardVisual;
        GameObject cardToPreview = null;

        if (effectAsset.CardType == CardType.Creature)
        {
            cardVisual = creatureCardTargetsEffect.GetComponent<CreatureCardVisual>();
            cardVisual.asset = effectAsset;
            cardVisual.LoadAssets();
            cardToPreview = creatureCardTargetsEffect;
        }

        if (effectAsset.CardType == CardType.Spell)
        {
            cardVisual = spellCardTargetsEffect.GetComponent<SpellCardVisual>();
            cardVisual.asset = effectAsset;
            cardVisual.LoadAssets();
            cardToPreview = spellCardTargetsEffect;
        }
        StartCoroutine(EffectPreview(cardToPreview, delay));
    }

    IEnumerator EffectPreview(GameObject card, float delay)
    {
        card.SetActive(true);
        yield return new WaitForSeconds(delay);
        card.SetActive(false);
        PreviewManager.instance.canPreview = true;
    }

}
