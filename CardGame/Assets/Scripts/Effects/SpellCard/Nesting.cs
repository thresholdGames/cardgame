﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Nesting : CardEffect
{

    public Nesting(CardLogic c) : base(c)
    {

    }

    public override void Activation()
    {

        new CMD_EffectPreview(card.asset);

        List<CardLogic> cards = new List<CardLogic>();
        for (int i = 0; i < card.owner.hand.hand.Count; i++)
        {
            if (card.owner.hand.hand[i].asset.CardType == CardType.Creature)
            {
                CreatureCardLogic ccl = card.owner.hand.hand[i] as CreatureCardLogic;
                if (ccl.cardAsset.type.type == Type.Dragon && ccl.ManaCost <= 2)
                {
                    cards.Add(ccl);
                }
            }
        }

        new CMD_PickTargets(cards, 1, "Pick a target to discard");

        new CMD_DiscardCard(card.owner);

        for (int i = 0; i < 2; i++)
        {
            new CMD_DrawCard(card.owner);
        }

    }

    public override bool Condition()
    {
        bool CanDiscard = false;
        bool hasCardsInDeck = card.owner.deck.deck.Count > 0;
        for (int i = 0; i < card.owner.hand.hand.Count; i++)
        {
            if (card.owner.hand.hand[i].asset.CardType == CardType.Creature)
            {
                CreatureCardLogic ccl = card.owner.hand.hand[i] as CreatureCardLogic;
                if (ccl.cardAsset.type.type == Type.Dragon && ccl.ManaCost <= 2)
                {
                    CanDiscard = true;
                    return CanDiscard & hasCardsInDeck;
                }
            }
        }

        
        return false;
    }
}
