﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OneMore : CardEffect
{
    public OneMore(CardLogic c) : base(c)
    {
    }

    public override bool Condition()
    {
        return card.owner.deck.deck.Count > 0;
    }

    public override void Activation()
    {
        base.Activation();
        new CMD_DrawCard(card.owner);
    }
}
