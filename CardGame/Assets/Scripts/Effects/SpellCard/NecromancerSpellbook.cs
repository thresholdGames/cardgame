﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public class NecromancerSpellbook : CardEffect
{
    public NecromancerSpellbook(CardLogic c) : base(c)
    {
    }

    public override bool Condition()
    {
        foreach (CreatureCardAsset cc in card.owner.graveyard.graveyard)
        {
            return true;
        }

        return false;
    }

    public override void Activation()
    {
        base.Activation();
        List<CardAsset> targets = new List<CardAsset>();
        foreach(CreatureCardAsset ca in card.owner.graveyard.graveyard)
        {
            targets.Add(ca);
        }
        new CMD_PickTargets(targets,1,"Pick a Creature to Revive");
        new CMD_Revive(card.owner);
    }
}
