﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CreatureEffect{

    public CreatureLogic creature;
    public CreatureCardLogic card;

    public CreatureEffect(CreatureCardLogic _card)
    {
        card = _card;
    }

    public virtual void Action() { }
    public virtual void Skill() { }
    public virtual void DeathWish() { }
    public virtual void OnTurnStart() { }
    public virtual void OnTurnEnd() { }
    public virtual bool ActionCondition() { return false; }
    public virtual bool SkillCondition() { return false; }
}
