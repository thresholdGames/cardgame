﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NoEffect_Card : CardEffect
{
    public NoEffect_Card(CardLogic c) : base(c)
    {
    }

    public override void Activation()
    {
    }

    public override bool Condition()
    {
        return false;
    }
}
