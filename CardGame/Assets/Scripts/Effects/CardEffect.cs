﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CardEffect{

    public CardLogic card;

    public CardEffect(CardLogic c)
    {
        card = c;
    }

    public virtual void Activation()
    {
        new CMD_EffectPreview(card.asset);
    }
    public virtual bool Condition() { return false; }

}
