﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EruptingDrake : CreatureEffect
{
    public EruptingDrake(CreatureCardLogic _card) : base(_card)
    {
    }

    public override void DeathWish()
    {
        List<CardAsset> targets = new List<CardAsset>();

        for (int i = 0; i < card.owner.graveyard.graveyard.Count; i++)
        {
            if (card.owner.graveyard.graveyard[i].CardType == CardType.Creature)
            {
                CreatureCardAsset ccl = card.owner.graveyard.graveyard[i] as CreatureCardAsset;
                if (ccl.type.type == Type.Dragon && ccl.ManaCost <= 2)
                {
                    targets.Add(ccl);
                }
            }
        }

        if(targets.Count > 0)
        {
            new CMD_EffectPreview(card.asset);
            new CMD_PickTargets(targets,1,"Pick 1 Creture to Revive");
            new CMD_Revive(card.owner);
        }
    }
}
