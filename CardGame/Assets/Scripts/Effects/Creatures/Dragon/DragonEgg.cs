﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DragonEgg : CreatureEffect
{
    public DragonEgg(CreatureCardLogic card) : base(card)
    {
    }

    public override void DeathWish()
    {
        new CMD_EffectPreview(creature.visual.asset);
        List<CardAsset> creatures = new List<CardAsset>();
        foreach (CardAsset creature in creature.owner.deck.deck)
        {
            if (creature.CardType == CardType.Creature)
            {
                CreatureCardAsset cca = (CreatureCardAsset)creature;
                if (cca.type.type == Type.Dragon)
                    creatures.Add(creature);
            }
        }

        if (creatures.Count > 0 && !creature.owner.hand.isFull())
        {
            new CMD_PickTargets(creatures, 1, "Pick 1 card to add to your deck");
            new CMD_AddCard(creature.owner);
        }     
    }
}
