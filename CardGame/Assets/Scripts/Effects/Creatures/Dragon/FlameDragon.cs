﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FlameDragon : CreatureEffect
{
    public FlameDragon(CreatureCardLogic card) : base(card)
    {
    }

    public override void Action()
    {
        new CMD_EffectPreview(creature.visual.asset);
        new CMD_PickTargets(creature.owner.enemy.field.field, 1, "Pick a target to deal 2 damage");
        new CMD_DealDamage(2);
    }

    public override bool ActionCondition()
    {
        bool DragonInField = false;
        bool CreatureInEnemy = card.owner.enemy.field.field.Count > 0;

        foreach (CreatureLogic creature in card.owner.field.field)
        {
            if (creature.visual.asset.type.type == Type.Dragon)
            {
                DragonInField = true;
                break;
            }
        }
        return DragonInField & CreatureInEnemy;
    }
}
