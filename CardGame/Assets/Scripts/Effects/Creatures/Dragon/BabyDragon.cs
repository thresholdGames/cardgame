﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BabyDragon : CreatureEffect
{
    public BabyDragon(CreatureCardLogic _card) : base(_card)
    {
    }

    public override void Skill()
    {
        creature.owner.PArea.AnimaVisual.CurrentCrystals -= 2;
        creature.owner.CurrentEssence -= 2;
        new CMD_EffectPreview(creature.visual.asset);
        new CMD_PickTargets(creature.owner.enemy.field.field, 1, "Pick a creature to deal 1 damage");
        new CMD_DealDamage(1);

    }

    public override bool SkillCondition()
    {
        bool hasEssence = false;
        bool isNotEmpty = false;
        if(creature.owner.CurrentEssence >= 2)
        {
            hasEssence = true;
        }

        if(creature.owner.enemy.field.field.Count > 0)
        {
            isNotEmpty = true;  
        }
        return hasEssence & isNotEmpty;
    }
}
