﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NoEffect : CreatureEffect
{
    public NoEffect(CreatureCardLogic _card) : base(_card)
    {
    }
}
