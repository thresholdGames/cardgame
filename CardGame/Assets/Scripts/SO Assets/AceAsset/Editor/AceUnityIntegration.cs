﻿using UnityEngine;
using UnityEditor;

static class AcerUnityIntegration {

	[MenuItem("Assets/Create/AceAsset")]
	public static void CreateYourScriptableObject() {
		ScriptableObjectUtility2.CreateAsset<AceAssset>();
	}

}
