﻿using UnityEngine;
using System.Collections;


public class AceAssset : ScriptableObject 
{
	public TypeAsset type;
    //public HoverPreview previewManager;
	public string ClassName;
	public int MaxHealth = 30;
	public string HeroPowerName;
	public Sprite AvatarImage;
    public Sprite HeroPowerIconImage;
    public Sprite AvatarBGImage;
    public Sprite HeroPowerBGImage;
    public Color32 AvatarBGTint;
    public Color32 HeroPowerBGTint;
}
