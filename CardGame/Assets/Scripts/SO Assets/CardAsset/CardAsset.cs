﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.Events;

public enum CardType
{
    None,
    Creature,
    Spell
}


public class CardAsset : ScriptableObject 
{
    public CardType CardType;
    public string CardName;
    [TextArea(2, 3)]
    public string Description;
	public Sprite CardImage;
    public int ManaCost;
}
