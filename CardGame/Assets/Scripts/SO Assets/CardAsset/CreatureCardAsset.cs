﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CreatureCardAsset : CardAsset
{
    [Header("Creature Info")]
    public TypeAsset type;
    public int Health;
    public int Attack;
    public int AttacksForOneTurn = 1;
} 
