﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

static class CreatureCardUnityIntegration
{

    [MenuItem("Assets/Create/CreatureCardAsset")]
    public static void CreateYourScriptableObject()
    {
        ScriptableObjectUtility2.CreateAsset<CreatureCardAsset>();
    }

}
