﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

static class SpellCardUnityIntegration
{

    [MenuItem("Assets/Create/SpellCardAsset")]
    public static void CreateYourScriptableObject()
    {
        ScriptableObjectUtility2.CreateAsset<SpellCardAsset>();
    }

}

