﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum Type
{
    Dragon,
    Warrior,
    Undead,
    Machine,
    Magic
}

public class TypeAsset : ScriptableObject {
    [Header("Type Info")]
    public Type type;
    public string typeName;
    public Color32 ClassCardTint;
    public Color32 ClassRibbonsTint;
}
