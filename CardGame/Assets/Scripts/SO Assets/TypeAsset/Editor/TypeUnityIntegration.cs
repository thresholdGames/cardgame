﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;


public class TypeUnityIntegration
{
    [MenuItem("Assets/Create/TypeAsset")]
    public static void CreateYourScriptableObject()
    {
        ScriptableObjectUtility2.CreateAsset<TypeAsset>();
    }
}
