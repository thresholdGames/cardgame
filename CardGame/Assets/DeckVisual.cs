﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DeckVisual : MonoBehaviour {

    public Text deckCount;

    public void DrawCard()
    {
        transform.position += Vector3.forward * 0.05f; 
    }

    public void UpdateDeckCount(float count)
    {
        deckCount.text = count.ToString();
    }
}
